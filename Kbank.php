<?php 
/**
* KasikornBank Class 2020
 *
 * @category  Payment Gateway
 * @author    ITCoder 
 * @version   1
**/
require('simple_html_dom.php');
header('Content-Type: application/json');
class KBANK{
	
	private $cookie = false;
	private $token = false;
	public $response = null;
	private $user = "";
	private $pass = "";
	
	public function Curl($method, $url, $header, $data, $cookie){
		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        //curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36');
        curl_setopt($ch, CURLOPT_USERAGENT, 'okhttp/3.8.0');
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        if($data){
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		}
		if($cookie){
			curl_setopt($ch, CURLOPT_COOKIESESSION, true);
			curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
			curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
		}
        return curl_exec($ch);
	}
	public function ebank(){
		$cookie = $this->genarate_cookie();
		$this->login($cookie);
		$ck = $this->get_created_cookie($cookie);
		$url = "https://online.kasikornbankgroup.com/K-Online/ib/redirectToIB.jsp?r=8643";
		$header = array(
		"accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"cache-control: no-cache",
		"content-type: application/x-www-form-urlencoded",
		"origin: https://online.kasikornbankgroup.com",
		"sec-fetch-dest: document",
		"sec-fetch-mode: navigate",
		"sec-fetch-site: same-origin",
		"sec-fetch-user: ?1",
		"upgrade-insecure-requests: 1",
		"Cookie:TS01001929=".$ck['TS01001929'].";BIGipServerRSSO=".$ck['BIGipServerRSSO'].";tok=".$ck['tok'].";JSESSIONID=".$ck['JSESSIONID'],
		"user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36"
		);
		$res = $this->Curl("GET",$url, $header,false,false);
		$this->del_cookie($cookie);
		$token = explode('value="',$res);
		$token = explode('" />',$token[1]);
		return $token[0];		
	}
	public function login_ebank($cookie){
		$token = $this->ebank();
		$header =  array(
			"content-type: application/x-www-form-urlencoded",
			);
		$url = "https://ebank.kasikornbankgroup.com/retail/security/Welcome.do";
		$data = "txtParam=".$token;
		return $res = $this->Curl("POST",$url, $header,$data,$cookie);
	}
	public function main_kbank($cookie){
		$url = "https://online.kasikornbankgroup.com/K-Online/indexHome.jsp";
		$header = array(
		"accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"cache-control: no-cache",
		"content-type: application/x-www-form-urlencoded",
		"origin: https://online.kasikornbankgroup.com",
		"sec-fetch-dest: document",
		"sec-fetch-mode: navigate",
		"sec-fetch-site: same-origin",
		"sec-fetch-user: ?1",
		"upgrade-insecure-requests: 1",
		"Cookie:TS01001929=".$cookie['TS01001929'].";BIGipServerRSSO=".$cookie['BIGipServerRSSO'].";tok=".$cookie['tok'].";JSESSIONID=".$cookie['JSESSIONID'],
		"user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36"
		);
		return  $this->Curl("GET",$url, $header,false,false);
		
	}
	public function get_balance_all($cookie){
		$url = "https://ebank.kasikornbankgroup.com/retail/cashmanagement/inquiry/AccountSummary.do?action=list_domain1";
		$header = array(
		"accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"cache-control: no-cache",
		"sec-fetch-dest: iframe",
		"sec-fetch-mode: navigate",
		"sec-fetch-site: same-origin",
		"sec-fetch-user: ?1",
		"Cookie:TS0195fd9d=".$cookie['TS0195fd9d'].";BIGipServer~EWeb~ibankgroup_pool=".$cookie['BIGipServerRSSO'].";JSESSIONID=".$cookie['JSESSIONID'],
		"upgrade-insecure-requests: 1",
		"user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36"
		);
		$res = $this->Curl("GET",$url, $header,false,false);
		$money = explode('<td bgcolor="D1F2CB"><b>',$res);
		$money = explode('</b>',$money[1]);
		return $money[0];
	}
	public function get_tran($cookie){
		$result = null;
		$arr = null;
		$url_main = "https://ebank.kasikornbankgroup.com/retail/cashmanagement/TodayAccountStatementInquiry.do";
		$header = array(
		"accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"cache-control: no-cache",
		"sec-fetch-dest: iframe",
		"sec-fetch-mode: navigate",
		"sec-fetch-site: same-origin",
		"sec-fetch-user: ?1",
		"Cookie:TS0195fd9d=".$cookie['TS0195fd9d'].";BIGipServer~EWeb~ibankgroup_pool=".$cookie['BIGipServerRSSO'].";JSESSIONID=".$cookie['JSESSIONID'],
		"upgrade-insecure-requests: 1",
		"user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36"
		);
		$res = $this->Curl("GET",$url_main, $header,false,false);
		$accID = explode('<option value="',$res);
		$accID = explode('">',$accID[2]);
		$accID = $accID[0];
		$token = explode('name="org.apache.struts.taglib.html.TOKEN" value="',$res);
		$token = explode('">',$token[1]);
		$token = $token[0];
		$url_trans = "https://ebank.kasikornbankgroup.com/retail/cashmanagement/TodayAccountStatementInquiry.do";
		$data = "acctId=".$accID."&action=detail&captcha_check=null&org.apache.struts.taglib.html.TOKEN=".$token."&st=0";
		$res_trans = $this->Curl("POST",$url_trans, $header,$data,false);
		$html = str_get_html($res_trans);
		$res_tran = '';
		foreach($html->find('table[rules=rows]') as $element){
			    $res_tran = $element->innertext;
		}
		preg_match_all('#<tr[^>]*>(.*?)</tr>#is', $res_tran, $lines);
		foreach ($lines[1] as $k => $v){
			preg_match_all('#<td[^>]*>(.*?)</td>#is', $v, $cell);
			    foreach ($cell[1] as $cell) {
				$result[$k][] = trim($cell);
				}
		}
        unset($result[0]);
		foreach ($result as $v){
			if($v[4] !=null){ 
			   $arr[] = str_replace("                                	<br>"," ",$v);			   
			}
		}
		return $arr;			         
	}
	public function login($cookie){
		$url = "https://online.kasikornbankgroup.com/K-Online/login.do";
		$header = array(
		"accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
		"cache-control: no-cache",
		"content-type: application/x-www-form-urlencoded",
		"origin: https://online.kasikornbankgroup.com",
		"sec-fetch-dest: document",
		"sec-fetch-mode: navigate",
		"sec-fetch-site: same-origin",
		"sec-fetch-user: ?1",
		"upgrade-insecure-requests: 1",
		"user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36"
		);
		$data = "app=0&cmd=authenticate&custType=&locale=th&password=".$this->pass."&tokenId=5555555&userName=".$this->user;
		return  $this->Curl("POST",$url, $header,$data,$cookie);
		
	}
	public function genarate_cookie(){
		$cookie_name = md5(microtime().mt_rand());
		//file_put_contents(dirname(__FILE__).'/'.$cookie_name, "");
		file_put_contents($cookie_name, "");
		$this->cookie = realpath($cookie_name);
		return $this->cookie;
	}
	public function get_created_cookie($cookie){
		if ($file = fopen($cookie, "r")) {
			while(!feof($file)) {
				$line = fgets($file);
				
				if(strpos($line, 'TS01001929')!==false){
					$TS01001929 = preg_replace("/\s+/", "", explode("TS01001929", $line)[1]);
				}
				if(strpos($line, 'BIGipServerRSSO')!==false){
					$BIGipServerRSSO = preg_replace("/\s+/", "", explode("BIGipServerRSSO", $line)[1]);
				}
				if(strpos($line, 'tok')!==false){
					$tok = preg_replace("/\s+/", "", explode("tok", $line)[1]);
				}
				if(strpos($line, 'JSESSIONID')!==false){
					$JSESSIONID = preg_replace("/\s+/", "", explode("JSESSIONID", $line)[1]);
				}
			}
			fclose($file);
		}
		if(empty($TS01001929)||empty($BIGipServerRSSO)||empty($tok)||empty($JSESSIONID)){
			return false;
		}else{
			return array(
				"TS01001929" => $TS01001929,
				"BIGipServerRSSO" => $BIGipServerRSSO,
				"tok"=>$tok,
				"JSESSIONID"=>$JSESSIONID
				
			);
		}
	}
	public function get_created_cookie_ebank($cookie){
		if ($file = fopen($cookie, "r")) {
			while(!feof($file)) {
				$line = fgets($file);
				
				if(strpos($line, 'TS0195fd9d')!==false){
					$TS01001929 = preg_replace("/\s+/", "", explode("TS0195fd9d", $line)[1]);
				}
				if(strpos($line, 'BIGipServer~EWeb~ibankgroup_pool')!==false){
					$BIGipServerRSSO = preg_replace("/\s+/", "", explode("BIGipServer~EWeb~ibankgroup_pool", $line)[1]);
				}
				if(strpos($line, 'JSESSIONID')!==false){
					$JSESSIONID = preg_replace("/\s+/", "", explode("JSESSIONID", $line)[1]);
				}
			}
			fclose($file);
		}
		if(empty($TS01001929)||empty($BIGipServerRSSO)||empty($JSESSIONID)){
			return false;
		}else{
			return array(
				"TS0195fd9d" => $TS01001929,
				"BIGipServerRSSO" => $BIGipServerRSSO,
				"JSESSIONID"=>$JSESSIONID
				
			);
		}
	}
	
	public function del_cookie($cookie){
		return unlink($cookie);
	}
}
?>